package com.example.android.sunshine.app;

import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new ForecastFragment())
                    .commit();
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class ForecastFragment extends Fragment {

        ArrayAdapter<String> forecastAdapter = null;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            inflater.inflate(R.menu.forecastfragments, menu);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_refresh) {
                FetchWeatherTask task = new FetchWeatherTask();
                task.execute("2128");
                return true;
            }

            return super.onOptionsItemSelected(item);
        }

        public ForecastFragment() {
        }

        public class FetchWeatherTask extends AsyncTask<String, Void, Object> {

            String[] forecastArray = null;

            @Override
            protected void onPostExecute(Object o) {
                if (forecastArray != null){
                    forecastAdapter.clear();
                    forecastAdapter.addAll(Arrays.asList(forecastArray));
                }
            }

            @Override
            protected String[] doInBackground(String... params) {
                // These two need to be declared outside the try/catch
                // so that they can be closed in the finally block.
                HttpURLConnection urlConnection = null;
                BufferedReader reader = null;

                // Will contain the raw JSON response as a string.
                String forecastJsonStr = null;

                try {
                    // Construct the URL for the OpenWeatherMap query
                    // Possible parameters are avaiable at OWM's forecast API page, at
                    // http://openweathermap.org/API#forecast
                    Uri uri =  Uri.parse(Constants.WEATHER_API_BASE_URL).buildUpon()
                            .appendQueryParameter(Constants.WEATHER_API_KEY_ZIP, params[0] + ",au")
                            .appendQueryParameter(Constants.WEATHER_API_KEY_UNITS, "metric")
                            .appendQueryParameter(Constants.WEATHER_API_KEY_COUNT, "7")
                            .appendQueryParameter(Constants.WEATHER_API_KEY_APPID, Constants.WEATHER_API_APPID)
                            .build();

                    Log.v(this.getClass().getSimpleName(), "URL : " + uri.toString());

                    URL url = new URL(
//                            "http://api.openweathermap.org/data/2.5/forecast/daily?zip=2128,au&units=metric&cnt=7&APPID=fdb5f1253ee3be6cd3edbd3f2f085d6b"
                            uri.toString()
                    );

                    // Create the request to OpenWeatherMap, and open the connection
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("GET");
                    urlConnection.connect();

                    // Read the input stream into a String
                    InputStream inputStream = urlConnection.getInputStream();
                    StringBuffer buffer = new StringBuffer();
                    if (inputStream == null) {
                        // Nothing to do.
                        return null;
                    }
                    reader = new BufferedReader(new InputStreamReader(inputStream));

                    String line;
                    while ((line = reader.readLine()) != null) {
                        // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                        // But it does make debugging a *lot* easier if you print out the completed
                        // buffer for debugging.
                        buffer.append(line + "\n");
                    }

                    if (buffer.length() == 0) {
                        // Stream was empty.  No point in parsing.
                        return null;
                    }
                    forecastJsonStr = buffer.toString();

                    Log.v(this.getClass().getSimpleName(), "Returned JSON : " + forecastJsonStr);
                    forecastArray = Util.getWeatherDataFromJson(forecastJsonStr, 7);
                    return forecastArray;

                } catch (JSONException jse) {
                    Log.e(this.getClass().getSimpleName(), "Error ", jse);
                    // If the code didn't successfully get the weather data, there's no point in attemping
                    // to parse it.
                    return null;
                }catch (IOException e) {
                    Log.e(this.getClass().getSimpleName(), "Error ", e);
                    // If the code didn't successfully get the weather data, there's no point in attemping
                    // to parse it.
                    return null;
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (final IOException e) {
                            Log.e(this.getClass().getSimpleName(), "Error closing stream", e);
                        }
                    }
                }

            }



        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.list_main, container, false);

            String[] temp = {"Mon - Fine - 23/11", "Tue - Cloudy - 25/16",
                    "Wed - Fine - 23/11", "Thu - Cloudy - 25/16",
                    "Fri - Fine - 23/11", "Sat - Cloudy - 25/16", "Sun - Fine - 23/11",
                    "Mon - Fine - 23/11", "Tue - Cloudy - 25/16",
                    "Mon - Fine - 23/11", "Tue - Cloudy - 25/16"};
            List<String> weeklyForecast = new ArrayList<String>(Arrays.asList(temp));

            forecastAdapter = new ArrayAdapter<String>(getActivity(),
                    R.layout.list_item, R.id.list_item, weeklyForecast);

            ((ListView) rootView.findViewById(R.id.list_view_forecast)).setAdapter(forecastAdapter);

            return rootView;

        }
    }
}
