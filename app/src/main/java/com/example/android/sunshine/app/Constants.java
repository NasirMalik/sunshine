package com.example.android.sunshine.app;

/**
 * Created by Nasir on 31/10/2016.
 */

public class Constants {

    public static final String WEATHER_API_BASE_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?";
    public static final String WEATHER_API_APPID = "fdb5f1253ee3be6cd3edbd3f2f085d6b";
    public static final String COUNTRY_AU = "au";

    public static final String WEATHER_API_KEY_UNITS = "units";
    public static final String WEATHER_API_KEY_ZIP = "zip";
    public static final String WEATHER_API_KEY_APPID = "APPID";
    public static final String WEATHER_API_KEY_COUNT = "cnt";

}
